import * as devJson from './conf/dev.json';
import * as prodJson from './conf/prod.json';

console.log('json: ', devJson);
console.log('env URL:', process.env.API_URL);
console.log('env ENV:', process.env.ENV);


const jsonDiv = document.createElement('div');
jsonDiv.textContent = process.env.ENV === 'dev' ? devJson.default.message : prodJson.default.message;
document.body.appendChild(jsonDiv);